<?php
/**
 * @file
 * This file holds the main Drupal hook functions 
 * and private functions for the mapstraction_cck_geocoder module.
 *
 * @ingroup Geo
 */

/**
 * Google geocoder URLs
 */
define('GOOGLE_GEOCODER_URL', 'http://maps.google.com/maps/api/geocode/json?');
define('GOOGLE_GEOCODER_STATUS_OK', 'OK');

/**
 * Implementation of hook_help()
 */
function mapstraction_cck_geocoder_help($path, $arg) {
  switch ($path) {
    case 'admin/help#mapstraction_cck_geocoder':
      $output = '<p>'. t('The Mapstraction CCK Geocoder module provides geocoding services to center the maps provided by the <a href="!map_url">Mapstraction CCK</a> module.',
        array('!map_url' =>'mapstraction_cck')) .'</p>';
    break;
  }
  return $output;
}

/**
 * Implementation of hook_theme().
 */
function mapstraction_cck_geocoder_theme() {
  return array(
    'mapstraction_cck_geocoder_result' => array(
      'arguments' => array('result' => NULL),
      'template' => 'mapstraction-cck-geocoder-result',
      'path' => drupal_get_path('module', 'mapstraction_cck_geocoder') .'/theme',
    ),
    'mapstraction_cck_geocoder_formatter_mapstraction_cckmapformatter_single_geocoder' => array(
      'arguments' => array('element' => NULL),
      'file' => 'theme/mapstraction_cck_geocoder.theme.inc',
    ),   
    'mapstraction_cck_geocoder_formatter_mapstraction_cckmapformatter_grouped_geocoder' => array(
      'arguments' => array('element' => NULL),
      'file' => 'theme/mapstraction_cck_geocoder.theme.inc',
    ),

  );
}

/**
 * Implementation of hook_menu()
 */
function mapstraction_cck_geocoder_menu() {
  $items['mapstraction_cck/geocoder'] = array(
    'page callback' => 'mapstraction_cck_geocoder_autocomplete',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Defines the Mapstraction CCK Geocoder module form
 * @param $form_state 
 * @param $map Map array to associate the geocoder to the map.
 * 
 * @return $form Associative array with form components
 */
function mapstraction_cck_geocoder_form_definition($form_state = NULL, $map = NULL){
  $form[$map[id].'_geocode_field'] = array(
    '#id' => 'mapstraction-cck-geocoder-'.$map[id].'-field',
    '#title' => t('Search address'),
    '#type' => 'textfield',
    '#autocomplete_path' => 'mapstraction_cck/geocoder',
    '#weight' => 1,
    '#attributes' => array(
      'geoautocomplete' => TRUE,
      'rel' => $map['id'],
    ),
    '#prefix' => "START<div style='clear:both' class='container-inline'>",
    
  );
  $form[$map[id].'_geocode_button'] = array(
    '#type' => 'item',
    '#value' => '<button type="button" id="mapstraction-cck-geocoder'.$map[id].'-button" rel="'.$map['id'].'">'.t('Geocode').'</button>',
    '#weight' => 2,
    '#suffix' => '</div>',
  );
  $form[$map[id].'_geocode_description'] = array(
    '#type' =>'item',
    '#weight' => 10,
    '#description' => t('You can center the map where you want, just type the place in this field'),
  	'#suffix' => 'END-FORM',
  );
  return $form;
}

/**
 * Location list from Google geocoding web service
 *
 * @param $query
 *    Address or location name
 * @return
 *    List of locations
 */
function mapstraction_cck_geocoder_autocomplete($query = '') {

  $locations = array();
  if ($response = mapstraction_cck_geocoder_response($query)) {
    foreach ($response as $location) {
      $locations[$location['address']] = theme('mapstraction_cck_geocoder_result', $location['components']);
    }
  }
  drupal_json($locations);
}

/**
 * Query Google geocoding web service
 * @param $address
 *    Address or location name
 * @return
 *    Array of placemarks
 */
function mapstraction_cck_geocoder_response($address) {

  $locations = $args = array();

  // The address that you want to geocode.
  $args['address'] = str_replace(' ', '+', $address);

  // The language in which to return results. If "language" is not supplied,
  // the geocoder will attempt to use the native language of the domain
  // from which the request is sent wherever possible.
  $language = language_default();
  $args['language'] = $language->language;

  // Response encoding.
  $args['oe'] = 'utf-8';

  //  Indicates whether or not the geocoding request comes from a device with a location sensor. This value must be either true or false.
  $args['sensor'] = 'false';

  //The textual latitude/longitude value for which you wish to obtain the closest, human-readable address.
  // $args['latlng'] = '40.714224,-3.961452';

  // The bounding box of the viewport within which to bias geocode results more prominently.
  // $args['bounds'] = '';

  // The region code, specified as a ccTLD ("top-level domain") two-character value.
  // $args['region'] = '';

  $query = http_build_query($args, '', '&');
  
  if (function_exists("curl_init")) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, GOOGLE_GEOCODER_URL . $query);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);
    curl_close($ch);  
  }
  else {
    $result = file_get_contents(GOOGLE_GEOCODER_URL . $query);
  }
  
  $response = json_decode($result);

  if ($response->status == GOOGLE_GEOCODER_STATUS_OK) {
    foreach ($response->results as $result) {
      $location = $components = array();
      foreach ($result->address_components as $component) {
        $key = $component->types[0];
        $components[$key] = $component->long_name;
        if ($key == 'country') {
          $components['country_code'] = $component->short_name;
        }
      }
      $components['street_address'] = $location['address'] = $result->formatted_address;
      $location['components'] = $components;
      $location['location'] = (array) $result->geometry->location;
      $location['bounds'] = (array) $result->geometry->viewport;
      $locations[] = $location;
    }
  }
  return $locations;
}

/**
 * Adds the Geocoder form to the map
 * 
 * @param $map Map that will be assocated to the geocoder
 * @param $provider Geocoding service provider
 */

function mapstraction_cck_geocoder_add_geocoder(&$map,$provider){
  mapstraction_cck_load_maps($provider);
  drupal_add_js(drupal_get_path('module', 'mapstraction_cck_geocoder')  .'/js/mapstraction_cck_geocoder.js');
  $myform = mapstraction_cck_geocoder_form_definition(NULL,$map);
  $form = drupal_render_form('mapstraction_cck_geocoder_form_definition',$myform);
  $form = substr($form,strpos($form, "START")+5);
  $form = substr($form,0,strpos($form,'END-FORM'));
  $map['geocode_theme'] = $form;
}

/**
 * Define the form fields for the geocoder settings form.
 * 
 * @param $defaults default values for the form
 * @return $form Fieldset that defines the geocoder settingd
 */
function mapstraction_cck_geocoder_settings($defaults){
  $form = array(
      '#type' => 'fieldset',
      '#title' => t('Geocoder Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['deprecated']= array(
  	  '#value' =>'<div id="mapstraction-cck-geocode-deprecated" class="messages error" style="display:none">'.
   	  t('You need the 1.1 version of IDELabMapstraction library if you want to use the geocoding service').'</div>',
    );
    $form['provider'] = array(
      '#type' => 'radios',
      '#title' => t('Geocode provider'),
      '#options' => array_merge(array('none' => t('None')),mapstraction_cck_available_geocode_providers()),
      '#description' => t('Select whether a field for geocode info to center the map in a determinate location is displayed'),
      '#required' => TRUE,
      '#default_value' => $defaults['geocode']['provider'],
    );
    $form['zoom'] = array(
      '#type' => 'textfield',
      '#title' => t('Zoom Level'),
      '#required' => TRUE,
      '#size' => 2,
      '#maxlength' => 2,
      '#description' => t('Select the zoom level for the map after it is centered in the desired location'),
      '#default_value' => $defaults['geocode']['zoom'],     
    );
    return $form;
}

/**
 * Implementation of hook_field_formatter_info().
 */
function mapstraction_cck_geocoder_field_formatter_info() {
  return array(
    // @@TODO: Implement Grouped Formatter.
    'mapstraction_cckmapformatter_grouped_geocoder' => array(
      'label' => t('Mapstraction CCK Grouped Map with Geocoder'),
      'field types' => array('geo'),
      'multiple values' => CONTENT_HANDLE_MODULE,
      'settings' => array('geocoder'=> TRUE)
    ),
    'mapstraction_cckmapformatter_single_geocoder' => array(
      'label' => t('Mapstraction CCK Single Map with Geocoder'),
      'field types' => array('geo'),
      'multiple values' => CONTENT_HANDLE_MODULE,
    ),
  );
}


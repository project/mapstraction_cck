<?php 
/**
 * Theme a fields using a map. Each field is given a map.
 */
function theme_mapstraction_cck_geocoder_formatter_mapstraction_cckmapformatter_single_geocoder($element) { 
  
  module_load_include('inc', 'mapstraction_cck', 'includes/mapstraction_cck.theme');
  return theme_mapstraction_cck_formatter_mapstraction_cckmapformatter_single($element, true);
}


/**
 * Theme a fields using a map. Every fields are showed in a map.
 */
function theme_mapstraction_cck_geocoder_formatter_mapstraction_cckmapformatter_grouped_geocoder($element) { 

  module_load_include('inc', 'mapstraction_cck', 'includes/mapstraction_cck.theme');
  return theme_mapstraction_cck_formatter_mapstraction_cckmapformatter_single($element, true);  
}
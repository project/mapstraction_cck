<?php

/**
 * @file: mapstraction-cck-geocoder-result.tpl.php
 *
 * Template file theming geocoder's response results.
 * 
 * @author Pablo López (plopesc)
 * @ingroup Geo
 */
?>
<span class="mapstraction-cck-geocoder-result detail-row-1"><?php print $result['street_address']; ?></span>
<span class="mapstraction-cck-geocoder-result detail-row-2"><?php print $result['postal_code'] .' '. $result['locality']; ?><?php print $result['locality'] ? ' - ' : ''; ?><?php print $result['country']; ?></span>